!-------------------------------------------------------------
! Module for creating perturbation
!   
! For more info see the README file and/or documentation.
!--------------------------------------------------------------


module pert 

USE const 
USE io
USE init

implicit none


CONTAINS 

!------------------------------------------------------------------------------
subroutine get_pert(tt,th,uu,vv,rho,pp,ff)
   !gets the perturbation and than balances the other fields
   real(kind=8), intent(inout), dimension(nx,ny,nz) :: uu,vv,tt,rho,pp,th
   real(kind=8), intent(in), dimension(ny) :: ff
   real(kind=8), dimension(nx,ny,nz) :: tt_pert,pp_pert
   real(kind=8) :: pp_old
   
   if ( add_t_pert ) then
      call create_pert(tt_pert)
      tt = tt + tt_pert
      th = tt*(p00/pp)**(Rd/cp) 
      pp_pert = 0.0
   else if ( add_p_pert ) then
      call create_pert(pp_pert)
      pp = pp + pp_pert
      tt_pert = 0.0
   end if
   
   ! Rebalance the perturbation hydrostatically and either
   ! (1) geostrophically, or
   ! (2) assuming gradient wind balance
   if ( rebalance.gt.0 ) then
      ! Hydrostatically adjust the temperature perturbation and update density
      if ( add_t_pert ) then
         do k=nz-1,2,-1
            do j=1,ny
               do i=1,nx
                  pp_old = pp(i,j,k-1)
                  pp(i,j,k-1) = pp(i,j,k+1) + 2*dz * pp(i,j,k)*g/(Rd*tt(i,j,k))
                  pp_pert(i,j,k-1) = pp(i,j,k-1) + pp_old
               enddo
            enddo
         enddo
         rho = pp/(Rd*tt)
      ! Hydrostatically adjust the pressure perturbation and update (potential) temperate
      else if ( add_p_pert ) then
         do i=1,nx
           do j=1,ny
             do k=2,nz-1
                rho(i,j,k) = -(pp(i,j,k+1)-pp(i,j,k-1))/(g*2.*dz)
             enddo
           enddo
         enddo
         tt = pp/(rho*Rd)
         th = tt*(p00/pp)**(Rd/cp)
      end if

      if ( rebalance == 1 ) then
         !approximate windfields using geostrophic approximation
         do k=1,nz
            do j=2,ny-1
               do i=2,nx-1
                  uu(i,j,k)=(-1/(rho(i,j,k)*ff(j))) *   &
                        ((pp(i,j+1,k)-pp(i,j-1,k))/(2*dy))
                  vv(i,j,k)=(1/(rho(i,j,k)*ff(j))) *   &
                        ((pp(i+1,j,k)-pp(i-1,j,k))/(2*dx))
                enddo
             enddo
         enddo
      else if ( rebalance == 2 ) then
         call gradient_balance(pp_pert, rho, ff, uu, vv)
      ! Invalid rebalance ID
      else 
         print *,'ERROR: rebalance ID. Valid values are 0-2.'
         stop 6
      end if
   else
      print*,"--> unbalanced perturbation <---"   
   endif
end subroutine get_pert
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
subroutine create_pert(pert)
   !define temperature or pressure anomaly by shape
   real(kind=8), intent(inout), dimension(nx,ny,nz) :: pert
   real(kind=8) :: radius

   do k=1,nz
      z = ((k-1)*dz - P_hgt)/P_z
      do j=1,ny
         y = ((j-P_ny-1)*dy)/P_rad
         do i=1,nx
            x = ((i-P_nx-1)*dx)/P_rad
            radius=sqrt(x*x+y*y)
              
            ! Gaussian perturbation
            if ( P_type==1 ) then   
               pert(i,j,k) = P_amp * exp(-0.5*(x*x+y*y+z*z))

            ! Localised cos-shaped perturbation 
            else if ( P_type==2 ) then
               if ( radius.lt.1 .and. abs(z).lt.1 ) then
                  pert(i,j,k) = P_amp * cos(pi*0.5*radius)**P_hexp * cos(pi*0.5*z)**2
               else
                  pert(i,j,k) = 0.0
               end if

            ! Localised polynomial added by Hai
            else if ( P_type == 3 ) then
               if ( radius.lt.1 .and. abs(z).lt.1 ) then
                  pert(i,j,k) = -P_amp * (radius**2 - 1)**3 * cos(pi*0.5*z)**2
               else
                  pert(i,j,k) = 0.0
               end if

            ! Invalid P_type
            else 
               print *,'ERROR: Unknown P_type. Valid values are 1-3.'
               stop 7
            endif

         enddo
      enddo
   enddo
end subroutine create_pert
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Recalcucate horizontal winds using gradient wind balance (by Hai)
subroutine gradient_balance(pp_pert, rho, ff, uu, vv)
    real(kind=8), intent(in) :: pp_pert(nx,ny,nz), rho(nx,ny,nz), ff(ny)
    real(kind=8), intent(inout), dimension(nx,ny,nz) :: uu,vv
    real(kind=8) :: r, cosa, sina, ug,vg,uvg, uv_gr

    do  k =1,nz
      do j = 1,ny
        y = (j-P_ny-1)*dy
        do i = 1,nx
           x = (i-P_nx-1)*dx
           r = sqrt(x*x+y*y)

           if ( r.gt.0 ) then
             cosa=x/r
             sina=y/r
             ug  = -1/(rho(i,j,k)*ff(j)) * (pp_pert(i,j+1,k)-pp_pert(i,j-1,k))/(2*dy)
             vg  = -1/(rho(i,j,k)*ff(j)) * (pp_pert(i+1,j,k)-pp_pert(i-1,j,k))/(2*dx)
             uvg = sqrt(ug**2 + vg**2)
             uv_gr = ( -r*ff(j) + sqrt( (r*ff(j))**2 + 4*r*ff(j)*uvg ) )/2.

             uu(i,j,k) = uu(i,j,k) - uv_gr*sina
             vv(i,j,k) = vv(i,j,k) + uv_gr*cosa
           endif
        enddo
      enddo
    enddo
end subroutine gradient_balance


end module pert
