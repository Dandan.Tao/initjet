!-------------------------------------------------------------
! Module defining the jet
!   
! For more info see the README file
!--------------------------------------------------------------


module jet

USE const 
USE io
USE init 

implicit none

! Mapping the six default jet_types onto horizontal and vertical types
!
! The default types are:
! 0: horizontally homogeneous winds
! 1: following Polvani-Esler (2007)
! 2: as (1) but, sinusoidal profile in the vertical with different shear in
!    troposphere/stratosphere (by Annick Terpstra)
! 3: Asymmetric jet profiles based on cos^2 in the horizontal and polynomial in
!    the vertical (developed by Hai Bui, old type 101)
! 4: Reverse-shear profiles, 1, 2 or 4 vertical segments (by Annick Terpstra)
! 5: LLJ setup, for example for CAOs (by Matthias Gottschalk)
integer, parameter :: jet_type_max = 5
integer, dimension(0:jet_type_max), parameter :: &
   & default_jet_type_hor = (/ 0, 1, 1, 5, 1, 4 /), &
   & default_jet_type_vert = (/ 4, 1, 2, 5, 4, 2 /)

CONTAINS 


!------------------------------------------------------------------------------
subroutine get_jet(uu_jet)
   real(kind=8), intent(out) :: uu_jet(ny,nz)
   real(kind=8) :: y, z, u_hor, u_vert
   integer :: j, k
   
   ! If the horizontal and vertical profiles have not been configured directly, 
   ! set them here based on an overall jet_type
   if ( jet_type_hor == -9 ) then
      if ( jet_type >= 0 .and. jet_type <= jet_type_max ) then
         jet_type_hor = default_jet_type_hor(jet_type)
      else
         print *,'ERROR: Unknown jet_type. Valid values are 0-',jet_type_max,'.'
         stop 1
      end if
   end if 
   if ( jet_type_vert == -9 ) then
      if ( jet_type >= 0 .and. jet_type <= jet_type_max ) then
         jet_type_vert = default_jet_type_vert(jet_type)
      else
         print *,'ERROR: Unknown jet_type. Valid values are 0-',jet_type_max,'.'
         stop 1
      end if
   end if 

   if (debug) then
      print *, 'Jet types: default, horizontal, vertical', jet_type, jet_type_hor, jet_type_vert
   end if
   
   do k=1,nz
      do j=1,ny
         ! Needs to be recalculated for every grid cell to allow for transformations
         z=(k-1)*dz - z_offset
         y=(j-(ny+1)/2.0)*dy

         if ( jet_core_slope .ne. 0.0 ) call transform_by_slope(y,z)
         if ( jet_tilt .ne. 0.0 ) call transform_by_tilt(y,z)

         u_hor = calc_h_profile(y)
         u_vert =  calc_v_profile(z)
         uu_jet(j,k) = u_max * u_hor * u_vert
      end do
   end do

   if (debug_profiles) call print_profile_j(uu_jet,dz)

end subroutine get_jet
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
function calc_h_profile(y) result (u_hor)
   real(kind=8), intent(in) :: y
   real(kind=8) :: u_hor 

   if ( jet_type_hor == 0 ) then 
      u_hor = 1.0
   else if ( jet_type_hor == 1 ) then
      u_hor = h_profile_polvani_esler(y)
   else if ( jet_type_hor == 2 ) then
      u_hor = h_profile_monotonous_shear(y)
   else if ( jet_type_hor == 3 ) then
      u_hor = h_profile_gaussian(y)
   else if ( jet_type_hor == 4 ) then
      u_hor = h_profile_homogeneous_core(y)
   else if ( jet_type_hor == 5 ) then
      u_hor = h_profile_asymmetric(y)
   else
      print *,'ERROR: Unknown jet_type_hor. Valid values are 0-5.'
      stop 2
   end if
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
function calc_v_profile(z) result (u_vert)
   real(kind=8), intent(in) :: z
   real(kind=8) :: u_vert

   if ( jet_type_vert == 0 ) then
      u_vert = 1.0
   else if ( jet_type_vert == 1 ) then
      u_vert = v_profile_polvani_esler(z)
   else if ( jet_type_vert == 2 ) then
      u_vert = v_profile_sin_2seg(z)
   else if ( jet_type_vert == 3 ) then
      u_vert = v_profile_gaussian(z)
   else if ( jet_type_vert == 4 ) then
      u_vert = v_profile_cos_1to4seg(z)
   else if ( jet_type_vert == 5 ) then
      u_vert = v_profile_polynomial(z)
   else if ( jet_type_vert == 6 ) then
      u_vert = v_profile_external(z)
   else
      print *,'ERROR: Unknown jet_type_vert. Valid values are 0-5.'
      stop 3
   end if
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
function h_profile_polvani_esler(y) result (u_hor)
   real(kind=8), intent(in) :: y
   real(kind=8) :: u_hor
   
   if (y .gt. -width/2.0 .and. y .lt. width/2.0) then
      u_hor = sin(pi*(sin(pi*(y-width/2)/(2*width))**2))**3
   else 
      u_hor = 0.0
   end if
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! monotonously positive xor negative shear in a sin-shaped transition
! from u_max2 (small y; "south") to u_max (large y; "north")
function h_profile_monotonous_shear(y) result (u_hor)
   real(kind=8), intent(in) :: y
   real(kind=8) :: u_hor

   if (y.lt.-width/2.0) then
      u_hor = 1.0
   elseif (y.gt.width/2.0) then
      u_hor = U_max2 / U_max
   else
      u_hor = 1.0 + (U_max2 - U_max)/U_max * (1 + sin(pi*y/width))/2.0
   endif
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Gaussian horizontal jet profile (for nh_llj == 2)
! Originally introduced for ice-edge jets by Stefan Keiderling
function h_profile_gaussian(y) result (u_hor)
   real(kind=8), intent(in) :: y
   real(kind=8) :: u_hor
   
   u_hor = exp(-0.5 * (abs(y)/width)**hexp)
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! A jet core with homogeneous winds of width width with sinusoidal flanks 
! on either side between width/2 and width2/2
! Adapted from Matthias Gottschalk (old type 8)
function h_profile_homogeneous_core(y) result (u_hor)
   real(kind=8), intent(in) :: y
   real(kind=8) :: u_hor
   
   ! Plateau at jet core
   if ( abs(y).lt.width/2.0 ) then
      u_hor = 1.0
   ! sinusoidal decrease on either side of the jet core
   elseif ( abs(y).lt.width2/2.0 ) then
      u_hor = (sin(pi*(cos(pi*(width2/2.0-abs(y))/(2*(width2-width))))**2))**hexp
   else
      u_hor = 0.0
   endif
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Asymmetric horizontal jet profile by Bui Hoang Hai (old type 101)
function h_profile_asymmetric(y) result (u_hor)
   real(kind=8), intent(in) :: y
   real(kind=8) :: ry, u_hor
   
   if ( y.le.0 ) then
      ry = y/width      ! Width on the equatorial side
   else
      ry = y/width2     ! Width on the poleward side
   endif
   
   if ( abs(ry).le.1.0 ) then 
      u_hor = cos(ry*pi/2)**2
   else
      u_hor = 0.0
   endif
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
function v_profile_polvani_esler(z) result (u_vert)
   real(kind=8), intent(in) :: z
   real(kind=8) :: u_vert
   
   u_vert = (z/hgt)*exp(-0.5*((z/hgt)**2-1))
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! sin-profile in the vertical with separte shear in troposphere/stratosphere
function v_profile_sin_2seg(z) result (u_vert)
   real(kind=8), intent(in) :: z
   real(kind=8) :: u_vert

   if (z.gt.0.0 .and. z.lt.hgt2) then
      if (z.gt.hgt) then
         u_vert = sin(pi/2*((hgt2-z)/(hgt2-hgt)))**vexp2
      else
         u_vert = sin(pi/2*(z/hgt))**vexp
      endif
   else
      u_vert=0.0
   endif
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Gaussian vertical jet profile (for vexp == 2)
! Originally introduced for ice-edge jets by Stefan Keiderling
function v_profile_gaussian(z) result (u_vert)
   real(kind=8), intent(in) :: z
   real(kind=8) :: u_vert
   
   u_vert = exp(-0.5 * (z/hgt)**vexp)
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! cos^n - shaped decay with height in 1-4 contiguous segments
! Phasing is adjusted such that the topmost layer always goes from U_max -> zero with increasing height
function v_profile_cos_1to4seg(z) result (u_vert)
   real(kind=8), intent(in) :: z
   real(kind=8) :: phase, u_vert
   logical :: lcontinue 
   
   ! True as soon as the first HXs is set, such that all lower segments are calculated irrespective of HXs
   lcontinue = .false. 
   ! To be increased by pi/2 for each calculated segment
   phase = 0.0
   
   ! If none of the conditions below apply, we're above or below the jet
   u_vert = 0.0

   ! Contains the old types 4-6 (reversed shear; 1, 2 or 4 segments)
   ! 
   ! Fourth segment in case of 4-segments (yields old type 8)
   if ( vexp4.gt.0 ) then
      if ( z.le.hgt4 .and. z.gt.hgt3 ) then
         u_vert = (cos( 0.5*pi*((z-hgt3)/(hgt4-hgt3)) ))**vexp4
      end if
      
      lcontinue = .true.
      phase = phase + pi/2.0
   end if
   ! Third segment in case of 3 or 4 segments
   if ( vexp3.gt.0 .or. lcontinue ) then
      if ( z.le.hgt3 .and. z.gt.hgt2 ) then
         u_vert = (cos( 0.5*pi*((z-hgt2)/(hgt3-hgt2)) - phase))**vexp3
      end if

      lcontinue = .true.
      phase = phase + pi/2.0
   end if
   ! Second segment in case of 2, 3 or 4 segment (yields old type 7 if both vexp3 and vexp4 <= 0)
   if ( vexp2.gt.0 .or. lcontinue ) then
       if ( z.le.hgt2 .and. z.gt.hgt ) then
          u_vert = (cos( 0.5*pi*((z-hgt)/(hgt2-hgt)) - phase))**vexp2
       end if

      phase = phase + pi/2.0
   end if
   !
   ! First segment is always calculated (yields old type 6 in isolation)
   if ( z.le.hgt .and. z .ge. 0.0 ) then 
      u_vert = (cos(0.5*pi*z/hgt - phase))**vexp
   end if
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! 3-Segment polynomial vertical wind profile by Hai Bui (used to be part of type 101)
function v_profile_polynomial(z) result (u_vert)
   real(kind=8), intent(in) :: z
   real(kind=8) :: u_vert
   real(kind=8) :: rz, alpha, a, w0, z0, Lzz
      
   ! rz varies from 0 (jet core) to 1 (jet egde)
   if (z<=hgt) then
      rz = (hgt - z)/hgt
   else
      rz = (z - hgt)/(hgt2-hgt)
   endif

   Lzz = 1.0 - lsmo2/2.
   w0 = 1./( 1. - (3./8.)*lsmo/Lzz)
   a = (3./4.)*w0*lsmo/Lzz
   alpha=w0/Lzz
      
   ! Fourth-order polynomial fit between jet core and lsmo
   if (rz<=lsmo) then
      u_vert = 1 + a*(rz/lsmo)**2 * (1./6.*(rz/lsmo)**2 - 1)
   ! Linear wind increase towards the core between lsmo and lsmo2
   else if (rz<=1.0-lsmo2) then
      u_vert = w0 - alpha*rz
   ! Fourth-order polynomial fit between lsmo2 and jet edge
   else if (rz<=1.0) then
      z0 = (1.0 - rz)/lsmo2
      u_vert = alpha*lsmo2/2.0 * z0**3 * (2.0 - z0)
   ! No wind outside the jet edge
   else
      u_vert = 0.0
   endif
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Read a wind profile and normalize by maximum wind
function v_profile_external(z) result (u_vert)
   real(kind=8), intent(in) :: z
   real(kind=8) :: u_vert
   
   print *,'ERROR: Reading vertical jet profiles is not yet implemented again.'
   stop 4
   ! TODO: 
   !  - On first call: read the entire profile, normalise, and store as module variable
   !    (may be best in IO module?)
   !  - On all calls: Interpolate to the requested height and return value
   ! 
   !call read_nc(ifile_uprof,u_vert)
   !u_vert(:) = u_vert(:) / maxval(u_vert)
end function
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Coordinate transform to yield a wind maximum following a tanh(y)-shape
! Addapted from an option added by Hai Bui
subroutine transform_by_slope(y,z)
   real(kind=8), intent(inout) :: y,z
   real(kind=8) :: z_offset

   z_offset = jet_core_slope * tanh(-y/width)
   z = z + z_offset
end subroutine
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Rotate the coordinate system to create a tilted jet
! Adapted from an option added by Hai Bui
subroutine transform_by_tilt(y,z)
   real(kind=8), intent(inout) :: y,z
   real(kind=8) :: y_offset

   y_offset = (z - hgt)*tan(jet_tilt*pi/180.) * dy/dz
   y = y + y_offset
end subroutine
!------------------------------------------------------------------------------


end module jet 
