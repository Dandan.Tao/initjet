!-------------------------------------------------------------
! Module for i/o
! input:  from jet.namelist
! output: 1) .bin (binary input file for WRFV3.4)
!         2) .nc 
!         3) .txt 
!
! For more info see the README file
!--------------------------------------------------------------


module io

USE const
USE netcdf

implicit none

!define namelist variables 
integer      :: nx,ny,nz,mnx,mny,imnx,imny, P_ny,P_nx,jet_type,jet_type_hor,jet_type_vert,BT_type,RH_type,P_type,bc_vert
real(kind=8) :: Lx,Ly,Lz,f,beta
real(kind=8) :: U_max,U_max2, width,width2, hexp, hgt,hgt2,hgt3,hgt4, vexp,vexp2,vexp3,vexp4, z_offset, lsmo,lsmo2, &
   &            jet_core_slope, jet_tilt
real(kind=8) :: p0,th0,tropo_hgt,N2_tr,N2_st, bc_type
real(kind=8) :: RH_max,RH_min,RH_hgt,RH_shape
real(kind=8) :: P_rad,P_z,P_amp,P_hgt,P_hexp
real(kind=8) :: N2_vals(6),N2_levs(6)  !Hai: For define N (use with bc_type=1),linear int. between levs
!Hai sst option
integer :: sfc_type, sfc_mosaic, wrf_bdr_depth, wrf_bdr_padding, rebalance
real(kind=8) :: sst_diff, sst_diff2, sfc_scale2, sfc_shift2

logical  :: sfc_adjust  ! If=false, SST = SST2 

character(len=48) :: ifile_uprof, sfc_ifile
logical   :: debug,debug_profiles,wrf_file,wrf_real_file,nc_file,add_moist,add_t_pert,add_p_pert,bouss
CONTAINS 

!---------------------------------------------------------------------------
subroutine get_nml()
!reads in the variables from the namelist-file (jet.namelist)
  integer :: ios
  namelist /domain/ nx,ny,nz,Lx,Ly,Lz,f,beta
  namelist /jet/ jet_type,jet_type_hor,jet_type_vert, jet_core_slope, jet_tilt, &
  &   U_max,U_max2, width,width2, hexp, hgt,hgt2,hgt3,hgt4, vexp,vexp2,vexp3,vexp4, z_offset, lsmo,lsmo2
  namelist /stab/ bc_type,p0,th0,tropo_hgt,N2_tr,N2_st,bc_vert,bouss,N2_vals,N2_levs
  namelist /moist/ add_moist,RH_type,RH_max,RH_min,RH_hgt,RH_shape
  namelist /pert/ add_t_pert,add_p_pert,P_type,P_rad,P_z,P_amp,P_nx,P_ny,P_hgt,P_hexp,rebalance
  namelist /prog/ debug,debug_profiles,wrf_file,nc_file,wrf_real_file
  namelist /sfc/ sfc_type,sfc_adjust, sst_diff,sst_diff2, sfc_shift2,sfc_scale2, wrf_bdr_padding,wrf_bdr_depth,sfc_ifile,sfc_mosaic

  ! Initialised only to have specific values marking if unset
  jet_type = -9
  jet_type_hor = -9
  jet_type_vert = -9

  !set defaults (will be overwritten by namelist if set)
  jet_core_slope = 0.0
  jet_tilt = 0.0

  U_max = 30.0
  U_max2 = 10.0

  width = 2.0e6
  width2 = 3.0e6
  hexp = 2.0

  hgt = 8.0e3
  hgt2 = 18.0e3
  vexp = 2.0
  vexp2 = 0.0
  vexp3 = 0.0
  vexp4 = 0.0

  z_offset = 0.0
  lsmo = 0.05
  lsmo2 = 0.05

  bouss=.false.
  beta=0.
  debug=.false.
  debug_profiles=.false.
  wrf_real_file=.false.
  bc_vert=0

  add_t_pert = .false.
  add_p_pert = .false.
  rebalance = 1

  sfc_type = 0
  sfc_adjust = .false.
  sfc_mosaic = 1
  wrf_bdr_depth = 5
  wrf_bdr_padding = 8
  sfc_ifile='initJET_sfc.nc'

  !open the namelist file
  open(unit=20,file='namelist.initJET',status='old',iostat=ios)
    if (ios /= 0) then
       print*,'ERROR: could not open namelist file'
       stop
    end if
    
  !read the data
  read(unit=20,nml=prog)
  read(unit=20,nml=domain)
  read(unit=20,nml=jet)
  read(unit=20,nml=sfc)
  read(unit=20,nml=stab)
  read(unit=20,nml=moist)
  read(unit=20,nml=pert)
  close(20)

  ! set some derived variables
  mnx = nx-1 ! Dimensions of the WRF unstaggered grid
  mny = ny-1
  ! Size of the inner domain, potentially a higher-resolution grid from which
  ! the actual sea ice concentration and sst is interpolated/averaged
  imnx = (mnx-wrf_bdr_padding*2)*sfc_mosaic
  imny = (mny-wrf_bdr_padding*2)*sfc_mosaic
end subroutine get_nml
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine write_wrf(uu,vv,th,rho,qq,ff,pp,sst,sst2)
   ! generate unformated fortran file 
   ! used as input for WRF ideal.exe,
   ! TODO: Verify why qq was changed to rh here (or if it even was changed this way)
   real(kind=8), intent(in), dimension(nx,ny,nz) :: uu,vv,th,rho,qq,pp
   real(kind=8), intent(in), dimension(mnx,mny) :: sst,sst2
   real(kind=8), intent(in), dimension(ny) :: ff
   
   if(debug)then
     print*,''
     print*,'--- WRITING OUTPUT .bin ---' 
   endif

   OPEN(unit=17, file='input_jet_3D', &
         form='unformatted',status='replace',convert='swap')  !for gfortran compiler
!   OPEN(unit=17, file='input_jet_3D', form='unformatted',status='replace')  !for pgf90 compiler
!   OPEN(unit=17, file='input_jet_3D', form='unformatted',status='replace',convert='BIG_ENDIAN')  !for cray fortran
   write(17)nx,ny,nz
   write(17)real(uu)
   write(17)real(vv)
   write(17)real(th)
   write(17)real(rho)
   write(17)real(qq)
   write(17)real(ff)
   write(17)real(sst)
   write(17)real(sst2)
   write(17)real(pp)  !Currently not used
   CLOSE(17)

   if(debug)then
      print*,'--- DONE WRITING OUTPUT .bin ---' 
   endif
end subroutine write_wrf
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
function to_m(a,nnz) result(res)
  ! From doubly-staggered initJET grid to WRF main grid
  integer, intent(in) :: nnz
  real(kind=8), intent(in), dimension(nx,ny,nnz) :: a
  real(kind=8), dimension(mnx,mny,nnz,1) :: res

  integer :: i,j,k

  do i = 1,mnx
     do j = 1,mny
        do k = 1,nnz
           res(i,j,k,1) = (a(i,j,k) + a(i+1,j,k) + a(i,j+1,k) + a(i+1,j+1,k))/4.0
        end do
     end do
  end do

end function
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
function to_m2(a) result(res)
  ! From doubly-staggered initJET grid to WRF main grid
  real(kind=8), intent(in), dimension(nx,ny) :: a
  real(kind=8), dimension(mnx,mny,1) :: res

  integer :: i,j

  do i = 1,mnx
     do j = 1,mny
        res(i,j,1) = (a(i,j) + a(i+1,j) + a(i,j+1) + a(i+1,j+1))/4.0
     end do
  end do

end function
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
function to_u(a,nnz) result(res)
  ! From doubly-staggered initJET grid to WRF x-staggerd grid
  integer, intent(in) :: nnz
  real(kind=8), intent(in) , dimension(nx,ny,nnz):: a
  real(kind=8) , dimension(nx,mny,nnz,1):: res

  integer :: i,j,k

  do i = 1,nx
     do j = 1,mny
        do k = 1,nnz
           res(i,j,k,1) = (a(i,j,k) + a(i,j+1,k))/2.0
        end do
     end do
  end do

end function
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
function to_u2(a) result(res)
  ! From doubly-staggered initJET grid to WRF main grid
  real(kind=8), intent(in), dimension(nx,ny) :: a
  real(kind=8) , dimension(nx,mny,1):: res

  integer :: i,j

  do i = 1,nx
     do j = 1,mny
        res(i,j,1) = (a(i,j) + a(i,j+1))/2.0
     end do
  end do

end function
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
function to_v(a,nnz) result(res)
  ! From doubly-staggered initJET grid to WRF y-staggerd grid
  integer, intent(in) :: nnz
  real(kind=8), intent(in), dimension(nx,ny,nnz) :: a
  real(kind=8), dimension(mnx,ny,nnz,1) :: res

  integer :: i,j,k

  do i = 1,mnx
     do j = 1,ny
        do k = 1,nnz
           res(i,j,k,1) = (a(i,j,k) + a(i+1,j,k))/2.0
        end do
     end do
  end do

end function
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
function to_v2(a) result(res)
  ! From doubly-staggered initJET grid to WRF main grid
  real(kind=8), intent(in), dimension(nx,ny) :: a
  real(kind=8), dimension(mnx,ny,1) :: res

  integer :: i,j

  do i = 1,mnx
     do j = 1,ny
        res(i,j,1) = (a(i,j) + a(i+1,j))/2.0
     end do
  end do

end function
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
function to_c2(a) result(res)
  ! From doubly-staggered initJET grid to WRF corner grid, only add length-1 dimensnsion
  real(kind=8), intent(in), dimension(nx,ny) :: a
  real(kind=8), dimension(nx,ny,1) :: res

  integer :: i,j

  do i = 1,nx
     do j = 1,ny
        res(i,j,1) = a(i,j)
     end do
  end do

end function
!---------------------------------------------------------------------------



!---------------------------------------------------------------------------
! Generate netCDF file mimicking the ones created by metgrid
! as used as input for WRF real.exe
subroutine write_wrf_real(uu,vv,tt,rh,zz,pp,ff,ee,sst,seaice,tsk)
  real(kind=8), parameter :: lat = 84.0, lon = 0.0, albedo = 0.75
  integer, parameter :: LU_waterID_USGS = 14, LU_waterID = 17, LU_iceID_USGS = 16, LU_iceID = 15

  real(kind=8), intent(in), dimension(nx,ny,nz) :: uu,vv,tt,rh,zz,pp
  real(kind=8), intent(in), dimension(mnx,mny) :: sst,seaice,tsk
  real(kind=8), intent(in), dimension(ny) :: ff,ee 

  real(kind=8), dimension(nx,ny) :: sfctemp
  real(kind=8), dimension(mnx,mny,1) :: sfcmtemp
  real(kind=8), dimension(mnx,mny,4,1) :: z0004temp
  real(kind=8), dimension(mnx,mny,12,1) :: z0012temp
  real(kind=8), dimension(mnx,mny,16,1) :: z0016temp
  real(kind=8), dimension(mnx,mny,21,1) :: z0021temp
  real(kind=8), dimension(mnx,mny,132,1) :: z0132temp
  
  character(len=3), parameter :: xyz = 'XYZ', xy = 'XY '
  character(len=19) :: datestr
  character(len=100) :: fname

  integer   :: ncid, mdimids(4), xdimids(4), ydimids(4), sfcmdimids(3), sfcxdimids(3), sfcydimids(3), &
     &         t_dimid, datestr_dimid, x_dimid, y_dimid, z_dimid, zst_dimid, zsm_dimid, xs_dimid, ys_dimid, &
     &         z0132_dimid, z0012_dimid, z0016_dimid, z0021_dimid
  integer   :: varid_Times, varid_PRES, varid_GHT, varid_SNOW, varid_SST, varid_SEAICE, varid_TSK, varid_PMSL, &
     &         varid_PSFC, varid_SOILHGT, varid_SOIL_LAYERS, varid_ST, varid_SM, varid_LANDSEA, & 
     &         varid_RH, varid_VV, varid_UU, varid_TT, &
     &         varid_URB_PARAM, varid_LAKE_DEPTH, varid_VAR_SSO, varid_OL4, varid_OL3, varid_OL2, &
     &         varid_OL1, varid_OA4, varid_OA3, varid_OA2, varid_OA1, varid_VAR, varid_CON, varid_SLOPECAT, &
     &         varid_SNOALB, varid_LAI12M, varid_GREENFRAC, varid_ALBEDO12M, varid_SCB_DOM, varid_SOILCBOT, &
     &         varid_SCT_DOM, varid_SOILCTOP, varid_SOILTEMP, varid_HGT_M, varid_LU_INDEX, varid_LANDUSEF, &
     &         varid_COSALPHA_V, varid_SINALPHA_V, varid_COSALPHA_U, varid_SINALPHA_U, varid_XLONG_C, &
     &         varid_XLAT_C, varid_LANDMASK, varid_COSALPHA, varid_SINALPHA, varid_F, varid_E, &
     &         varid_MAPFAC_UY, varid_MAPFAC_VY, varid_MAPFAC_MY, varid_MAPFAC_UX, varid_MAPFAC_VX, & 
     &         varid_MAPFAC_MX, varid_MAPFAC_U, varid_MAPFAC_V, varid_MAPFAC_M, varid_CLONG, varid_CLAT, &
     &         varid_XLONG_U, varid_XLAT_U, varid_XLONG_V, varid_XLAT_V, varid_XLONG_M, varid_XLAT_M
  
  integer   :: i, j, day, hour
  
  if(debug)then
    print*,''
    print*,'--- WRITING real WRF met_em-like OUTPUT ---' 
  endif


  do day = 1,4
  do hour = 0,23,24
  
  write(datestr, '(A8,I0.2,A1,I0.2,A6)') '2000-01-', day, '_', hour, ':00:00'
  write(*,*) datestr
  fname = 'met_em.d01.'//datestr//'.nc'

  !create netcdf
  call check( nf90_create(fname, or(NF90_CLOBBER, NF90_64BIT_OFFSET), ncid) )

  !define dimensions
  call check( nf90_def_dim(ncid, "Time", NF90_UNLIMITED, t_dimid) )
  call check( nf90_def_dim(ncid, "DateStrLen", 19, datestr_dimid) )
  call check( nf90_def_dim(ncid, "west_east", mnx, x_dimid) )
  call check( nf90_def_dim(ncid, "south_north", mny, y_dimid) )
  call check( nf90_def_dim(ncid, "num_metgrid_levels", nz, z_dimid) )
  call check( nf90_def_dim(ncid, "num_st_levels", 4, zst_dimid) )
  call check( nf90_def_dim(ncid, "num_sm_levels", 4, zsm_dimid) )
  call check( nf90_def_dim(ncid, "west_east_stag", nx, xs_dimid) )
  call check( nf90_def_dim(ncid, "south_north_stag", ny, ys_dimid) )
  call check( nf90_def_dim(ncid, "z-dimension0132", 132, z0132_dimid) )
  call check( nf90_def_dim(ncid, "z-dimension0012",  12, z0012_dimid) )
  call check( nf90_def_dim(ncid, "z-dimension0016",  16, z0016_dimid) )
  call check( nf90_def_dim(ncid, "z-dimension0021",  21, z0021_dimid) )
  mdimids = (/ x_dimid, y_dimid, z_dimid, t_dimid /)
  xdimids = (/ xs_dimid, y_dimid, z_dimid, t_dimid /)
  ydimids = (/ x_dimid, ys_dimid, z_dimid, t_dimid /)
  sfcmdimids = (/ x_dimid, y_dimid, t_dimid  /)
  sfcxdimids = (/ xs_dimid, y_dimid, t_dimid  /)
  sfcydimids = (/ x_dimid, ys_dimid, t_dimid  /)

  !define variables
  call check( nf90_def_var(ncid, "Times", NF90_CHAR, (/ datestr_dimid, t_dimid /), varid_Times) )

  call check( nf90_def_var(ncid, "PRES", NF90_REAL, mdimids, varid_PRES) )
  call check( nf90_put_att(ncid, varid_PRES, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_PRES, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_PRES, 'units', '') )
  call check( nf90_put_att(ncid, varid_PRES, 'description', '') )
  call check( nf90_put_att(ncid, varid_PRES, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_PRES, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_PRES, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "GHT", NF90_REAL, mdimids, varid_GHT) )
  call check( nf90_put_att(ncid, varid_GHT, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_GHT, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_GHT, 'units', 'm') )
  call check( nf90_put_att(ncid, varid_GHT, 'description', 'Height') )
  call check( nf90_put_att(ncid, varid_GHT, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_GHT, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_GHT, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SNOW", NF90_REAL, sfcmdimids, varid_SNOW) )
  call check( nf90_put_att(ncid, varid_SNOW, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SNOW, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SNOW, 'units', 'kg m-2') )
  call check( nf90_put_att(ncid, varid_SNOW, 'description', 'Water Equivalent of Accumulated Snow Depth') )
  call check( nf90_put_att(ncid, varid_SNOW, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SNOW, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SNOW, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SST", NF90_REAL, sfcmdimids, varid_SST) )
  call check( nf90_put_att(ncid, varid_SST, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SST, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SST, 'units', 'K') )
  call check( nf90_put_att(ncid, varid_SST, 'description', 'Sea-Surface Temperature') )
  call check( nf90_put_att(ncid, varid_SST, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SST, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SST, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SEAICE", NF90_REAL, sfcmdimids, varid_SEAICE) )
  call check( nf90_put_att(ncid, varid_SEAICE, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SEAICE, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SEAICE, 'units', '0/1 Flag') )
  call check( nf90_put_att(ncid, varid_SEAICE, 'description', 'Sea-Ice-Flag') )
  call check( nf90_put_att(ncid, varid_SEAICE, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SEAICE, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SEAICE, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SKINTEMP", NF90_REAL, sfcmdimids, varid_TSK) )
  call check( nf90_put_att(ncid, varid_TSK, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_TSK, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_TSK, 'units', 'K') )
  call check( nf90_put_att(ncid, varid_TSK, 'description', 'Surface skin temperature') )
  call check( nf90_put_att(ncid, varid_TSK, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_TSK, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_TSK, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "PMSL", NF90_REAL, sfcmdimids, varid_PMSL) )
  call check( nf90_put_att(ncid, varid_PMSL, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_PMSL, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_PMSL, 'units', 'Pa') )
  call check( nf90_put_att(ncid, varid_PMSL, 'description', 'Sea-level Pressure') )
  call check( nf90_put_att(ncid, varid_PMSL, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_PMSL, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_PMSL, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "PSFC", NF90_REAL, sfcmdimids, varid_PSFC) )
  call check( nf90_put_att(ncid, varid_PSFC, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_PSFC, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_PSFC, 'units', 'Pa') )
  call check( nf90_put_att(ncid, varid_PSFC, 'description', 'Surface Pressure') )
  call check( nf90_put_att(ncid, varid_PSFC, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_PSFC, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_PSFC, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SOILHGT", NF90_REAL, sfcmdimids, varid_SOILHGT) )
  call check( nf90_put_att(ncid, varid_SOILHGT, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SOILHGT, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SOILHGT, 'units', 'm') )
  call check( nf90_put_att(ncid, varid_SOILHGT, 'description', 'Terrain field of source analysis') )
  call check( nf90_put_att(ncid, varid_SOILHGT, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SOILHGT, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SOILHGT, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SOIL_LAYERS", NF90_REAL, (/ x_dimid, y_dimid, zst_dimid, t_dimid /), varid_SOIL_LAYERS) )
  call check( nf90_put_att(ncid, varid_SOIL_LAYERS, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SOIL_LAYERS, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_SOIL_LAYERS, 'units', '') )
  call check( nf90_put_att(ncid, varid_SOIL_LAYERS, 'description', '') )
  call check( nf90_put_att(ncid, varid_SOIL_LAYERS, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SOIL_LAYERS, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SOIL_LAYERS, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "ST", NF90_REAL, (/ x_dimid, y_dimid, zst_dimid, t_dimid /), varid_ST) )
  call check( nf90_put_att(ncid, varid_ST, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_ST, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_ST, 'units', '') )
  call check( nf90_put_att(ncid, varid_ST, 'description', '') )
  call check( nf90_put_att(ncid, varid_ST, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_ST, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_ST, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SM", NF90_REAL, (/ x_dimid, y_dimid, zsm_dimid, t_dimid /), varid_SM) )
  call check( nf90_put_att(ncid, varid_SM, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SM, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_SM, 'units', '') )
  call check( nf90_put_att(ncid, varid_SM, 'description', '') )
  call check( nf90_put_att(ncid, varid_SM, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SM, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SM, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "LANDSEA", NF90_REAL, sfcmdimids, varid_LANDSEA) )
  call check( nf90_put_att(ncid, varid_LANDSEA, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_LANDSEA, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_LANDSEA, 'units', '0/1 Flag') )
  call check( nf90_put_att(ncid, varid_LANDSEA, 'description', 'Land/Sea flag') )
  call check( nf90_put_att(ncid, varid_LANDSEA, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_LANDSEA, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_LANDSEA, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "RH", NF90_REAL, mdimids, varid_RH) )
  call check( nf90_put_att(ncid, varid_RH, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_RH, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_RH, 'units', '%') )
  call check( nf90_put_att(ncid, varid_RH, 'description', 'Relative Humidity') )
  call check( nf90_put_att(ncid, varid_RH, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_RH, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_RH, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "VV", NF90_REAL, ydimids, varid_VV) )
  call check( nf90_put_att(ncid, varid_VV, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_VV, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_VV, 'units', 'm s-1') )
  call check( nf90_put_att(ncid, varid_VV, 'description', 'V') )
  call check( nf90_put_att(ncid, varid_VV, 'stagger', 'V') )
  call check( nf90_put_att(ncid, varid_VV, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_VV, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "UU", NF90_REAL, xdimids, varid_UU) )
  call check( nf90_put_att(ncid, varid_UU, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_UU, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_UU, 'units', 'm s-1') )
  call check( nf90_put_att(ncid, varid_UU, 'description', 'U') )
  call check( nf90_put_att(ncid, varid_UU, 'stagger', 'U') )
  call check( nf90_put_att(ncid, varid_UU, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_UU, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "TT", NF90_REAL, mdimids, varid_TT) )
  call check( nf90_put_att(ncid, varid_TT, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_TT, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_TT, 'units', 'K') )
  call check( nf90_put_att(ncid, varid_TT, 'description', 'Temperature') )
  call check( nf90_put_att(ncid, varid_TT, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_TT, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_TT, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "URB_PARAM", NF90_REAL, (/ x_dimid, y_dimid, z0132_dimid, t_dimid /), varid_URB_PARAM) )
  call check( nf90_put_att(ncid, varid_URB_PARAM, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_URB_PARAM, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_URB_PARAM, 'units', 'dimensionless') )
  call check( nf90_put_att(ncid, varid_URB_PARAM, 'description', 'Urban_Parameters') )
  call check( nf90_put_att(ncid, varid_URB_PARAM, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_URB_PARAM, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_URB_PARAM, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "LAKE_DEPTH", NF90_REAL, sfcmdimids, varid_LAKE_DEPTH) )
  call check( nf90_put_att(ncid, varid_LAKE_DEPTH, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_LAKE_DEPTH, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_LAKE_DEPTH, 'units', 'meters MSL') )
  call check( nf90_put_att(ncid, varid_LAKE_DEPTH, 'description', 'Topography height') )
  call check( nf90_put_att(ncid, varid_LAKE_DEPTH, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_LAKE_DEPTH, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_LAKE_DEPTH, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "VAR_SSO", NF90_REAL, sfcmdimids, varid_VAR_SSO) )
  call check( nf90_put_att(ncid, varid_VAR_SSO, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_VAR_SSO, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_VAR_SSO, 'units', 'meters2 MSL') )
  call check( nf90_put_att(ncid, varid_VAR_SSO, 'description', 'Variance of Subgrid Scale Orography') )
  call check( nf90_put_att(ncid, varid_VAR_SSO, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_VAR_SSO, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_VAR_SSO, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "OL4", NF90_REAL, sfcmdimids, varid_OL4) )
  call check( nf90_put_att(ncid, varid_OL4, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_OL4, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_OL4, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_OL4, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_OL4, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_OL4, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_OL4, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "OL3", NF90_REAL, sfcmdimids, varid_OL3) )
  call check( nf90_put_att(ncid, varid_OL3, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_OL3, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_OL3, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_OL3, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_OL3, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_OL3, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_OL3, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "OL2", NF90_REAL, sfcmdimids, varid_OL2) )
  call check( nf90_put_att(ncid, varid_OL2, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_OL2, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_OL2, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_OL2, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_OL2, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_OL2, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_OL2, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "OL1", NF90_REAL, sfcmdimids, varid_OL1) )
  call check( nf90_put_att(ncid, varid_OL1, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_OL1, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_OL1, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_OL1, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_OL1, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_OL1, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_OL1, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "OA4", NF90_REAL, sfcmdimids, varid_OA4) )
  call check( nf90_put_att(ncid, varid_OA4, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_OA4, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_OA4, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_OA4, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_OA4, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_OA4, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_OA4, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "OA3", NF90_REAL, sfcmdimids, varid_OA3) )
  call check( nf90_put_att(ncid, varid_OA3, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_OA3, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_OA3, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_OA3, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_OA3, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_OA3, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_OA3, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "OA2", NF90_REAL, sfcmdimids, varid_OA2) )
  call check( nf90_put_att(ncid, varid_OA2, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_OA2, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_OA2, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_OA2, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_OA2, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_OA2, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_OA2, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "OA1", NF90_REAL, sfcmdimids, varid_OA1) )
  call check( nf90_put_att(ncid, varid_OA1, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_OA1, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_OA1, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_OA1, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_OA1, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_OA1, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_OA1, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "VAR", NF90_REAL, sfcmdimids, varid_VAR) )
  call check( nf90_put_att(ncid, varid_VAR, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_VAR, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_VAR, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_VAR, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_VAR, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_VAR, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_VAR, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "CON", NF90_REAL, sfcmdimids, varid_CON) )
  call check( nf90_put_att(ncid, varid_CON, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_CON, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_CON, 'units', 'whoknows') )
  call check( nf90_put_att(ncid, varid_CON, 'description', 'something') )
  call check( nf90_put_att(ncid, varid_CON, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_CON, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_CON, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SLOPECAT", NF90_REAL, sfcmdimids, varid_SLOPECAT) )
  call check( nf90_put_att(ncid, varid_SLOPECAT, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SLOPECAT, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SLOPECAT, 'units', 'category') )
  call check( nf90_put_att(ncid, varid_SLOPECAT, 'description', 'Dominant category') )
  call check( nf90_put_att(ncid, varid_SLOPECAT, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SLOPECAT, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SLOPECAT, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SNOALB", NF90_REAL, sfcmdimids, varid_SNOALB) )
  call check( nf90_put_att(ncid, varid_SNOALB, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SNOALB, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SNOALB, 'units', 'percent') )
  call check( nf90_put_att(ncid, varid_SNOALB, 'description', 'Maximum snow albedo') )
  call check( nf90_put_att(ncid, varid_SNOALB, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SNOALB, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SNOALB, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "LAI12M", NF90_REAL, (/ x_dimid, y_dimid, z0012_dimid, t_dimid /), varid_LAI12M) )
  call check( nf90_put_att(ncid, varid_LAI12M, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_LAI12M, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_LAI12M, 'units', 'm^2/m^2') )
  call check( nf90_put_att(ncid, varid_LAI12M, 'description', 'MODIS LAI') )
  call check( nf90_put_att(ncid, varid_LAI12M, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_LAI12M, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_LAI12M, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "GREENFRAC", NF90_REAL, (/ x_dimid, y_dimid, z0012_dimid, t_dimid /), varid_GREENFRAC) )
  call check( nf90_put_att(ncid, varid_GREENFRAC, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_GREENFRAC, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_GREENFRAC, 'units', 'fraction') )
  call check( nf90_put_att(ncid, varid_GREENFRAC, 'description', 'MODIS FPAR') )
  call check( nf90_put_att(ncid, varid_GREENFRAC, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_GREENFRAC, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_GREENFRAC, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "ALBEDO12M", NF90_REAL, (/ x_dimid, y_dimid, z0012_dimid, t_dimid /), varid_ALBEDO12M) )
  call check( nf90_put_att(ncid, varid_ALBEDO12M, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_ALBEDO12M, 'MemoryOrder', xyz) )
  call check( nf90_put_att(ncid, varid_ALBEDO12M, 'units', 'percent') )
  call check( nf90_put_att(ncid, varid_ALBEDO12M, 'description', 'Monthly surface albedo') )
  call check( nf90_put_att(ncid, varid_ALBEDO12M, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_ALBEDO12M, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_ALBEDO12M, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SCB_DOM", NF90_REAL, sfcmdimids, varid_SCB_DOM) )
  call check( nf90_put_att(ncid, varid_SCB_DOM, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SCB_DOM, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SCB_DOM, 'units', 'category') )
  call check( nf90_put_att(ncid, varid_SCB_DOM, 'description', 'Dominant category') )
  call check( nf90_put_att(ncid, varid_SCB_DOM, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SCB_DOM, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SCB_DOM, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SOILCBOT", NF90_REAL, (/ x_dimid, y_dimid, z0016_dimid, t_dimid /), varid_SOILCBOT) )
  call check( nf90_put_att(ncid, varid_SOILCBOT, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SOILCBOT, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SOILCBOT, 'units', 'category') )
  call check( nf90_put_att(ncid, varid_SOILCBOT, 'description', '16-category top-layer soil type') )
  call check( nf90_put_att(ncid, varid_SOILCBOT, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SOILCBOT, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SOILCBOT, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SCT_DOM", NF90_REAL, sfcmdimids, varid_SCT_DOM) )
  call check( nf90_put_att(ncid, varid_SCT_DOM, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SCT_DOM, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SCT_DOM, 'units', 'category') )
  call check( nf90_put_att(ncid, varid_SCT_DOM, 'description', 'Dominant category') )
  call check( nf90_put_att(ncid, varid_SCT_DOM, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SCT_DOM, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SCT_DOM, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SOILCTOP", NF90_REAL, (/ x_dimid, y_dimid, z0016_dimid, t_dimid /), varid_SOILCTOP) )
  call check( nf90_put_att(ncid, varid_SOILCTOP, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SOILCTOP, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SOILCTOP, 'units', 'category') )
  call check( nf90_put_att(ncid, varid_SOILCTOP, 'description', '16-category top-layer soil type') )
  call check( nf90_put_att(ncid, varid_SOILCTOP, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SOILCTOP, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SOILCTOP, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SOILTEMP", NF90_REAL, sfcmdimids, varid_SOILTEMP) )
  call check( nf90_put_att(ncid, varid_SOILTEMP, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SOILTEMP, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SOILTEMP, 'units', 'Kelvin') )
  call check( nf90_put_att(ncid, varid_SOILTEMP, 'description', 'Annual mean deep soil temperature') )
  call check( nf90_put_att(ncid, varid_SOILTEMP, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SOILTEMP, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SOILTEMP, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "HGT_M", NF90_REAL, sfcmdimids, varid_HGT_M) )
  call check( nf90_put_att(ncid, varid_HGT_M, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_HGT_M, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_HGT_M, 'units', 'meters MSL') )
  call check( nf90_put_att(ncid, varid_HGT_M, 'description', 'GMTED2010 30-arc-second topography height') )
  call check( nf90_put_att(ncid, varid_HGT_M, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_HGT_M, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_HGT_M, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "LU_INDEX", NF90_REAL, sfcmdimids, varid_LU_INDEX) )
  call check( nf90_put_att(ncid, varid_LU_INDEX, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_LU_INDEX, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_LU_INDEX, 'units', 'category') )
  call check( nf90_put_att(ncid, varid_LU_INDEX, 'description', 'Dominant category') )
  call check( nf90_put_att(ncid, varid_LU_INDEX, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_LU_INDEX, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_LU_INDEX, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "LANDUSEF", NF90_REAL, (/ x_dimid, y_dimid, z0021_dimid, t_dimid /), varid_LANDUSEF) )
  call check( nf90_put_att(ncid, varid_LANDUSEF, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_LANDUSEF, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_LANDUSEF, 'units', 'category') )
  call check( nf90_put_att(ncid, varid_LANDUSEF, 'description', 'Noah-modified 21-category IGBP-MODIS landuse') )
  call check( nf90_put_att(ncid, varid_LANDUSEF, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_LANDUSEF, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_LANDUSEF, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "COSALPHA_V", NF90_REAL, sfcydimids, varid_COSALPHA_V) )
  call check( nf90_put_att(ncid, varid_COSALPHA_V, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_COSALPHA_V, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_COSALPHA_V, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_COSALPHA_V, 'description', 'Cosine of rotation angle on V grid') )
  call check( nf90_put_att(ncid, varid_COSALPHA_V, 'stagger', 'V') )
  call check( nf90_put_att(ncid, varid_COSALPHA_V, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_COSALPHA_V, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SINALPHA_V", NF90_REAL, sfcydimids, varid_SINALPHA_V) )
  call check( nf90_put_att(ncid, varid_SINALPHA_V, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SINALPHA_V, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SINALPHA_V, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_SINALPHA_V, 'description', 'Sine of rotation angle on V grid') )
  call check( nf90_put_att(ncid, varid_SINALPHA_V, 'stagger', 'V') )
  call check( nf90_put_att(ncid, varid_SINALPHA_V, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SINALPHA_V, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "COSALPHA_U", NF90_REAL, sfcxdimids, varid_COSALPHA_U) )
  call check( nf90_put_att(ncid, varid_COSALPHA_U, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_COSALPHA_U, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_COSALPHA_U, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_COSALPHA_U, 'description', 'Cosine of rotation angle on U grid') )
  call check( nf90_put_att(ncid, varid_COSALPHA_U, 'stagger', 'U') )
  call check( nf90_put_att(ncid, varid_COSALPHA_U, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_COSALPHA_U, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SINALPHA_U", NF90_REAL, sfcxdimids, varid_SINALPHA_U) )
  call check( nf90_put_att(ncid, varid_SINALPHA_U, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SINALPHA_U, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SINALPHA_U, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_SINALPHA_U, 'description', 'Sine of rotation angle on U grid') )
  call check( nf90_put_att(ncid, varid_SINALPHA_U, 'stagger', 'U') )
  call check( nf90_put_att(ncid, varid_SINALPHA_U, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SINALPHA_U, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "XLONG_C", NF90_REAL, (/ xs_dimid, ys_dimid, t_dimid /), varid_XLONG_C) )
  call check( nf90_put_att(ncid, varid_XLONG_C, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_XLONG_C, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_XLONG_C, 'units', 'degrees longitude') )
  call check( nf90_put_att(ncid, varid_XLONG_C, 'description', 'Longitude at grid cell corners') )
  call check( nf90_put_att(ncid, varid_XLONG_C, 'stagger', 'CORNER') )
  call check( nf90_put_att(ncid, varid_XLONG_C, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_XLONG_C, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "XLAT_C", NF90_REAL, (/ xs_dimid, ys_dimid, t_dimid /), varid_XLAT_C) )
  call check( nf90_put_att(ncid, varid_XLAT_C, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_XLAT_C, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_XLAT_C, 'units', 'degrees latitude') )
  call check( nf90_put_att(ncid, varid_XLAT_C, 'description', 'Latitude at grid cell corners') )
  call check( nf90_put_att(ncid, varid_XLAT_C, 'stagger', 'CORNER') )
  call check( nf90_put_att(ncid, varid_XLAT_C, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_XLAT_C, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "LANDMASK", NF90_REAL, sfcmdimids, varid_LANDMASK) )
  call check( nf90_put_att(ncid, varid_LANDMASK, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_LANDMASK, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_LANDMASK, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_LANDMASK, 'description', 'Landmask : 1=land, 0=water') )
  call check( nf90_put_att(ncid, varid_LANDMASK, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_LANDMASK, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_LANDMASK, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "COSALPHA", NF90_REAL, sfcmdimids, varid_COSALPHA) )
  call check( nf90_put_att(ncid, varid_COSALPHA, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_COSALPHA, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_COSALPHA, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_COSALPHA, 'description', 'Cosine of rotation angle') )
  call check( nf90_put_att(ncid, varid_COSALPHA, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_COSALPHA, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_COSALPHA, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "SINALPHA", NF90_REAL, sfcmdimids, varid_SINALPHA) )
  call check( nf90_put_att(ncid, varid_SINALPHA, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_SINALPHA, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_SINALPHA, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_SINALPHA, 'description', 'Sine of rotation angle') )
  call check( nf90_put_att(ncid, varid_SINALPHA, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_SINALPHA, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_SINALPHA, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "F", NF90_REAL, sfcmdimids, varid_F) )
  call check( nf90_put_att(ncid, varid_F, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_F, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_F, 'units', '-') )
  call check( nf90_put_att(ncid, varid_F, 'description', 'Coriolis F parameter') )
  call check( nf90_put_att(ncid, varid_F, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_F, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_F, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "E", NF90_REAL, sfcmdimids, varid_E) )
  call check( nf90_put_att(ncid, varid_E, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_E, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_E, 'units', '-') )
  call check( nf90_put_att(ncid, varid_E, 'description', 'Coriolis E parameter') )
  call check( nf90_put_att(ncid, varid_E, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_E, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_E, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_UY", NF90_REAL, sfcxdimids, varid_MAPFAC_UY) )
  call check( nf90_put_att(ncid, varid_MAPFAC_UY, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_UY, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_UY, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_UY, 'description', 'Mapfactor (y-dir) on U grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_UY, 'stagger', 'U') )
  call check( nf90_put_att(ncid, varid_MAPFAC_UY, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_UY, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_VY", NF90_REAL, sfcydimids, varid_MAPFAC_VY) )
  call check( nf90_put_att(ncid, varid_MAPFAC_VY, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_VY, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_VY, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_VY, 'description', 'Mapfactor (y-dir) on V grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_VY, 'stagger', 'V') )
  call check( nf90_put_att(ncid, varid_MAPFAC_VY, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_VY, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_MY", NF90_REAL, sfcmdimids, varid_MAPFAC_MY) )
  call check( nf90_put_att(ncid, varid_MAPFAC_MY, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_MY, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_MY, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_MY, 'description', 'Mapfactor (y-dir) on mass grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_MY, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_MAPFAC_MY, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_MY, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_UX", NF90_REAL, sfcxdimids, varid_MAPFAC_UX) )
  call check( nf90_put_att(ncid, varid_MAPFAC_UX, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_UX, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_UX, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_UX, 'description', 'Mapfactor (x-dir) on U grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_UX, 'stagger', 'U') )
  call check( nf90_put_att(ncid, varid_MAPFAC_UX, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_UX, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_VX", NF90_REAL, sfcydimids, varid_MAPFAC_VX) )
  call check( nf90_put_att(ncid, varid_MAPFAC_VX, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_VX, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_VX, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_VX, 'description', 'Mapfactor (x-dir) on V grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_VX, 'stagger', 'V') )
  call check( nf90_put_att(ncid, varid_MAPFAC_VX, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_VX, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_MX", NF90_REAL, sfcmdimids, varid_MAPFAC_MX) )
  call check( nf90_put_att(ncid, varid_MAPFAC_MX, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_MX, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_MX, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_MX, 'description', 'Mapfactor (x-dir) on mass grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_MX, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_MAPFAC_MX, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_MX, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_U", NF90_REAL, sfcxdimids, varid_MAPFAC_U) )
  call check( nf90_put_att(ncid, varid_MAPFAC_U, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_U, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_U, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_U, 'description', 'Mapfactor on U grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_U, 'stagger', 'U') )
  call check( nf90_put_att(ncid, varid_MAPFAC_U, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_U, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_V", NF90_REAL, sfcydimids, varid_MAPFAC_V) )
  call check( nf90_put_att(ncid, varid_MAPFAC_V, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_V, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_V, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_V, 'description', 'Mapfactor on V grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_V, 'stagger', 'V') )
  call check( nf90_put_att(ncid, varid_MAPFAC_V, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_V, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "MAPFAC_M", NF90_REAL, sfcmdimids, varid_MAPFAC_M) )
  call check( nf90_put_att(ncid, varid_MAPFAC_M, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_MAPFAC_M, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_MAPFAC_M, 'units', 'none') )
  call check( nf90_put_att(ncid, varid_MAPFAC_M, 'description', 'Mapfactor on mass grid') )
  call check( nf90_put_att(ncid, varid_MAPFAC_M, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_MAPFAC_M, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_MAPFAC_M, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "CLONG", NF90_REAL, sfcmdimids, varid_CLONG) )
  call check( nf90_put_att(ncid, varid_CLONG, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_CLONG, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_CLONG, 'units', 'degrees longitude') )
  call check( nf90_put_att(ncid, varid_CLONG, 'description', 'Computational longitude on mass grid') )
  call check( nf90_put_att(ncid, varid_CLONG, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_CLONG, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_CLONG, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "CLAT", NF90_REAL, sfcmdimids, varid_CLAT) )
  call check( nf90_put_att(ncid, varid_CLAT, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_CLAT, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_CLAT, 'units', 'degrees latitude') )
  call check( nf90_put_att(ncid, varid_CLAT, 'description', 'Computational latitude on mass grid') )
  call check( nf90_put_att(ncid, varid_CLAT, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_CLAT, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_CLAT, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "XLONG_U", NF90_REAL, sfcxdimids, varid_XLONG_U) )
  call check( nf90_put_att(ncid, varid_XLONG_U, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_XLONG_U, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_XLONG_U, 'units', 'degrees longitude') )
  call check( nf90_put_att(ncid, varid_XLONG_U, 'description', 'Longitude on U grid') )
  call check( nf90_put_att(ncid, varid_XLONG_U, 'stagger', 'U') )
  call check( nf90_put_att(ncid, varid_XLONG_U, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_XLONG_U, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "XLAT_U", NF90_REAL, sfcxdimids, varid_XLAT_U) )
  call check( nf90_put_att(ncid, varid_XLAT_U, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_XLAT_U, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_XLAT_U, 'units', 'degrees latitude') )
  call check( nf90_put_att(ncid, varid_XLAT_U, 'description', 'Latitude on U grid') )
  call check( nf90_put_att(ncid, varid_XLAT_U, 'stagger', 'U') )
  call check( nf90_put_att(ncid, varid_XLAT_U, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_XLAT_U, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "XLONG_V", NF90_REAL, sfcydimids, varid_XLONG_V) )
  call check( nf90_put_att(ncid, varid_XLONG_V, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_XLONG_V, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_XLONG_V, 'units', 'degrees longitude') )
  call check( nf90_put_att(ncid, varid_XLONG_V, 'description', 'Longitude on V grid') )
  call check( nf90_put_att(ncid, varid_XLONG_V, 'stagger', 'V') )
  call check( nf90_put_att(ncid, varid_XLONG_V, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_XLONG_V, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "XLAT_V", NF90_REAL, sfcydimids, varid_XLAT_V) )
  call check( nf90_put_att(ncid, varid_XLAT_V, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_XLAT_V, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_XLAT_V, 'units', 'degrees latitude') )
  call check( nf90_put_att(ncid, varid_XLAT_V, 'description', 'Latitude on V grid') )
  call check( nf90_put_att(ncid, varid_XLAT_V, 'stagger', 'V') )
  call check( nf90_put_att(ncid, varid_XLAT_V, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_XLAT_V, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "XLONG_M", NF90_REAL, sfcmdimids, varid_XLONG_M) )
  call check( nf90_put_att(ncid, varid_XLONG_M, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_XLONG_M, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_XLONG_M, 'units', 'degrees longitude') )
  call check( nf90_put_att(ncid, varid_XLONG_M, 'description', 'Longitude on mass grid') )
  call check( nf90_put_att(ncid, varid_XLONG_M, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_XLONG_M, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_XLONG_M, 'sr_y', 1) )

  call check( nf90_def_var(ncid, "XLAT_M", NF90_REAL, sfcmdimids, varid_XLAT_M) )
  call check( nf90_put_att(ncid, varid_XLAT_M, 'FieldType', 104) )
  call check( nf90_put_att(ncid, varid_XLAT_M, 'MemoryOrder', xy) )
  call check( nf90_put_att(ncid, varid_XLAT_M, 'units', 'degrees latitude') )
  call check( nf90_put_att(ncid, varid_XLAT_M, 'description', 'Latitude on mass grid') )
  call check( nf90_put_att(ncid, varid_XLAT_M, 'stagger', 'M') )
  call check( nf90_put_att(ncid, varid_XLAT_M, 'sr_x', 1) )
  call check( nf90_put_att(ncid, varid_XLAT_M, 'sr_y', 1) )

  ! Global attributes
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'TITLE', 'OUTPUT FROM initJET') )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'SIMULATION_START_DATE', datestr) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'WEST-EAST_GRID_DIMENSION', nx) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'SOUTH-NORTH_GRID_DIMENSION', ny) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'BOTTOM-TOP_GRID_DIMENSION', nz) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'WEST-EAST_PATCH_START_UNSTAG', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'WEST-EAST_PATCH_END_UNSTAG', mnx) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'WEST-EAST_PATCH_START_STAG', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'WEST-EAST_PATCH_END_STAG', nx) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'SOUTH-NORTH_PATCH_START_UNSTAG', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'SOUTH-NORTH_PATCH_END_UNSTAG', mny) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'SOUTH-NORTH_PATCH_START_STAG', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'SOUTH-NORTH_PATCH_END_STAG', ny) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'GRIDTYPE', 'C') )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'DX', Lx/mnx ) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'DY', Ly/mny ) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'DYN_OPT', 2) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'CEN_LAT', lat) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'CEN_LON', lon) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'TRUELAT1', lat) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'TRUELAT2', lat) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'MOAD_CEN_LAT', lat) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'STAND_LON', lon) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'POLE_LAT', 90.0) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'POLE_LON', 0.0) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'MAP_PROJ', 0) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'MMINLU', 'MODIFIED_IGBP_MODIS_NOAH') )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'NUM_LAND_CAT', 21) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'ISWATER', LU_waterID) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'ISLAKE', 21) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'ISICE', LU_iceID) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'ISURBAN', 13) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'ISOILWATER', 14) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'grid_id', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'parent_id', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'i_parent_start', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'j_parent_start', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'i_parent_end', nx) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'j_parent_end', ny) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'parent_grid_ratio', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'sr_x', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'sr_y', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'NUM_METGRID_SOIL_LEVELS', 4) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_METGRID', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_EXCLUDED_MIDDLE', 0) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_SNOW', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_PSFC', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_SLP', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_SST', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_SOILHGT', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_SOIL_LAYERS', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_MF_XY', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_LAI12M', 1) )
  call check( nf90_put_att(ncid, NF90_GLOBAL, 'FLAG_LAKE_DEPTH', 1) )

  ! finish file definition
  call check( nf90_enddef(ncid) )
  
  ! write data
  call check( nf90_put_var(ncid, varid_Times, (/ datestr /)) )

  call check( nf90_put_var(ncid, varid_PRES, to_m(pp,nz)) )
  call check( nf90_put_var(ncid, varid_GHT, to_m(zz,nz)) )
  call check( nf90_put_var(ncid, varid_SST, reshape(sst, (/ mnx,mny,1 /))) )
  call check( nf90_put_var(ncid, varid_SEAICE, reshape(seaice, (/ mnx,mny,1 /))) )
  call check( nf90_put_var(ncid, varid_TSK, reshape(tsk, (/ mnx,mny,1 /))) )
  call check( nf90_put_var(ncid, varid_PMSL, to_m2(pp(:,:,1))) )
  call check( nf90_put_var(ncid, varid_PSFC, to_m2(pp(:,:,1))) )
  call check( nf90_put_var(ncid, varid_RH, to_m(rh*100,nz)) )
  call check( nf90_put_var(ncid, varid_VV, to_v(vv,nz)) )
  call check( nf90_put_var(ncid, varid_UU, to_u(uu,nz)) )
  call check( nf90_put_var(ncid, varid_TT, to_m(tt,nz)) )

  sfctemp(:,:) = 0.0
  z0012temp(:,:,:,:) = 0.0
  z0132temp(:,:,:,:) = 0.0
  call check( nf90_put_var(ncid, varid_LANDMASK, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_LANDSEA, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_SNOW, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_SOILHGT, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_SOILTEMP, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_ST, z0004temp) )
  call check( nf90_put_var(ncid, varid_LAKE_DEPTH, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_VAR_SSO, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_OL4, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_OL3, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_OL2, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_OL1, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_OA4, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_OA3, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_OA2, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_OA1, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_VAR, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_CON, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_SLOPECAT, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_HGT_M, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_SINALPHA_V, to_v2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_SINALPHA_U, to_u2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_SINALPHA, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_URB_PARAM, z0132temp) )
  call check( nf90_put_var(ncid, varid_LAI12M, z0012temp) )
  call check( nf90_put_var(ncid, varid_GREENFRAC, z0012temp) )
 
  sfctemp(:,:) = 1.0
  z0004temp(:,:,:,:) = 1.0
  call check( nf90_put_var(ncid, varid_SM, z0004temp) )
  call check( nf90_put_var(ncid, varid_COSALPHA_V, to_v2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_COSALPHA_U, to_u2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_COSALPHA, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_UY, to_u2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_VY, to_v2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_MY, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_UX, to_u2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_VX, to_v2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_MX, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_U, to_u2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_V, to_v2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_MAPFAC_M, to_m2(sfctemp)) )

  sfctemp(:,:) = albedo
  z0012temp(:,:,:,:) = albedo
  call check( nf90_put_var(ncid, varid_SNOALB, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_ALBEDO12M, z0012temp) )

  sfctemp(:,:) = lat
  call check( nf90_put_var(ncid, varid_XLAT_C, to_c2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_CLAT, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_XLAT_U, to_u2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_XLAT_V, to_v2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_XLAT_M, to_m2(sfctemp)) )

  sfctemp(:,:) = lon
  call check( nf90_put_var(ncid, varid_XLONG_C, to_c2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_CLONG, to_m2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_XLONG_U, to_u2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_XLONG_V, to_v2(sfctemp)) )
  call check( nf90_put_var(ncid, varid_XLONG_M, to_m2(sfctemp)) )
  
  do i = 1,mnx
     do j = 1,mny
        if ( seaice(i,j) >= 0.5 ) then
           sfcmtemp(i,j,1) = LU_iceID_USGS
        else
           sfcmtemp(i,j,1) = LU_waterID_USGS
        end if
     end do
  end do
  call check( nf90_put_var(ncid, varid_SCB_DOM, sfcmtemp) )
  call check( nf90_put_var(ncid, varid_SCT_DOM, sfcmtemp) )

  do i = 1,mnx
     do j = 1,mny
        if ( seaice(i,j) >= 0.5 ) then
           sfcmtemp(i,j,1) = LU_iceID
        else
           sfcmtemp(i,j,1) = LU_waterID
        end if
     end do
  end do
  call check( nf90_put_var(ncid, varid_LU_INDEX, sfcmtemp) )

  z0016temp(:,:,:,:) = 0.0
  z0021temp(:,:,:,:) = 0.0
  z0016temp(:,:,LU_waterID_USGS,:) = 1.0
  z0021temp(:,:,LU_waterID,:) = 1.0
  call check( nf90_put_var(ncid, varid_SOILCBOT, z0016temp) )
  call check( nf90_put_var(ncid, varid_SOILCTOP, z0016temp) )
  call check( nf90_put_var(ncid, varid_LANDUSEF, z0021temp) )
   
  ! Standard depths of soil layer interfaces
  z0004temp(:,:,1,:) = 289.0
  z0004temp(:,:,2,:) = 100.0
  z0004temp(:,:,3,:) = 28.0
  z0004temp(:,:,4,:) = 7.0
  call check( nf90_put_var(ncid, varid_SOIL_LAYERS, z0004temp) )

  do i = 1,nx
     sfctemp(i,:) = ff(:)
  end do
  call check( nf90_put_var(ncid, varid_F, to_m2(sfctemp)) )

  do i = 1,nx
     sfctemp(i,:) = ee(:)
  end do
  call check( nf90_put_var(ncid, varid_E, to_m2(sfctemp)) )

  !close the file
  call check( nf90_close(ncid) )
  
  end do ! loop hours
  end do ! loop days

end subroutine write_wrf_real
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine write_nc(uu,pp,rho,tt,rh,th,thv,ttv,qq,zz,ff,sst,sst2,seaice,seaice2,tsk)
!write 2D version of the jet to jet.nc
  real(kind=8),intent(in),dimension(ny,nz) :: uu,pp,rho,tt,rh,th,thv,ttv,qq,zz
  real(kind=8),intent(in),dimension(mnx,mny) :: sst, sst2, seaice, seaice2, tsk
  real(kind=8),intent(in),dimension(ny) :: ff
  character(len=*), parameter :: fname = "initJET.nc"
  integer, parameter          :: ndims=2
  integer                     :: ncid, dimids(ndims),dimny(1),z_dimid,y_dimid,x_dimid,dimidsm2(ndims), &
        &                        xm_dimid, ym_dimid
  integer   :: varid_u,varid_p,varid_rho,varid_tt,varid_rh,varid_th,varid_thv,varid_ttv,varid_qq,varid_zz, &
        &      varid_f, varid_ff,varid_sst,varid_sst2,varid_seaice,varid_seaice2,varid_tsk, &
        &      varid_dy,varid_dz,varid_dx
  real(kind=8)::dy,dz,dx

  if(debug)then
    print*,''
    print*,'--- WRITING OUTPUT .NC ---' 
  endif

  !oeps cannot access dy...
  dy=Ly/mny
  dz=Lz/(nz-1)

  !create netcdf
  call check( nf90_create(fname, or(NF90_CLOBBER, NF90_64BIT_OFFSET), ncid) )

  !define dimensions
  call check( nf90_def_dim(ncid, "z", nz, z_dimid) )
  call check( nf90_def_dim(ncid, "y", ny, y_dimid) )
  call check( nf90_def_dim(ncid, "x", nx, x_dimid) )
  call check( nf90_def_dim(ncid, "ym", mny, ym_dimid) )
  call check( nf90_def_dim(ncid, "xm", mnx, xm_dimid) )

  dimids =  (/ y_dimid, z_dimid /)
  dimidsm2=  (/ xm_dimid, ym_dimid /)
  dimny=y_dimid
  !define data
  call check( nf90_def_var(ncid, "f",  NF90_REAL, varid_f) )
  call check( nf90_def_var(ncid, "dx", NF90_REAL, varid_dx) )
  call check( nf90_def_var(ncid, "dy", NF90_REAL, varid_dy) )
  call check( nf90_def_var(ncid, "dz", NF90_REAL, varid_dz) )
  call check( nf90_def_var(ncid, "ff", NF90_REAL, dimny,varid_ff) )
  call check( nf90_def_var(ncid, "sst",NF90_REAL, dimidsm2,varid_sst) )
  call check( nf90_def_var(ncid, "sst2",NF90_REAL, dimidsm2,varid_sst2) )
  call check( nf90_def_var(ncid, "seaice",NF90_REAL, dimidsm2,varid_seaice) )
  call check( nf90_def_var(ncid, "seaice2",NF90_REAL, dimidsm2,varid_seaice2) )
  call check( nf90_def_var(ncid, "tsk",NF90_REAL, dimidsm2,varid_tsk) )
  call check( nf90_def_var(ncid, "uu", NF90_REAL, dimids, varid_u) )
  call check( nf90_def_var(ncid, "pp", NF90_REAL, dimids, varid_p) )
  call check( nf90_def_var(ncid, "rho",NF90_REAL, dimids, varid_rho) )
  call check( nf90_def_var(ncid, "tt", NF90_REAL, dimids, varid_tt) )
  call check( nf90_def_var(ncid, "rh", NF90_REAL, dimids, varid_rh) )
  call check( nf90_def_var(ncid, "th", NF90_REAL, dimids, varid_th) )
  call check( nf90_def_var(ncid, "thv",NF90_REAL, dimids, varid_thv) )
  call check( nf90_def_var(ncid, "ttv",NF90_REAL, dimids, varid_ttv) )
  call check( nf90_def_var(ncid, "qq", NF90_REAL, dimids, varid_qq) )
  call check( nf90_def_var(ncid, "zz", NF90_REAL, dimids, varid_zz) )
  call check( nf90_enddef(ncid) )

  !write the data
  call check( nf90_put_var(ncid, varid_f,  f)  )
  call check( nf90_put_var(ncid, varid_ff,  ff)  )
  call check( nf90_put_var(ncid, varid_sst,  sst)  )
  call check( nf90_put_var(ncid, varid_sst2,  sst2)  )
  call check( nf90_put_var(ncid, varid_seaice,  seaice)  )
  call check( nf90_put_var(ncid, varid_seaice2,  seaice2)  )
  call check( nf90_put_var(ncid, varid_tsk,  tsk)  )
  call check( nf90_put_var(ncid, varid_dx, dx) )
  call check( nf90_put_var(ncid, varid_dy, dy) )
  call check( nf90_put_var(ncid, varid_dz, dz) )
  call check( nf90_put_var(ncid, varid_u,  uu) )
  call check( nf90_put_var(ncid, varid_p,  pp) )
  call check( nf90_put_var(ncid, varid_rho,rho))
  call check( nf90_put_var(ncid, varid_tt, tt) )
  call check( nf90_put_var(ncid, varid_rh, rh) )
  call check( nf90_put_var(ncid, varid_th, th) )
  call check( nf90_put_var(ncid, varid_thv,thv))
  call check( nf90_put_var(ncid, varid_ttv,ttv))
  call check( nf90_put_var(ncid, varid_qq, qq) )
  call check( nf90_put_var(ncid, varid_zz, zz) )
  !close the file
  call check( nf90_close(ncid) )

  if(debug)then
     print*,'--- DONE WRITING OUTPUT .NC ---' 
  endif

end subroutine write_nc
!---------------------------------------------------------------------------



!---------------------------------------------------------------------------
subroutine read_nc(ifile,prof)
!read in a single vertical profile from ifile .nc
character(len=*),intent(in) :: ifile
real(kind=8),intent(out),dimension(nz) :: prof
integer :: ncid, varid, dimid, nprof

!open netcdf
call check( nf90_open(ifile, NF90_NOWRITE, ncid) )

!get the dimension and allocate prof-array
call check( nf90_inq_dimid(ncid, "ncl1", dimid) ) !assume name of dim is from ncl...
call check( nf90_inquire_dimension(ncid, dimid, len = nprof))
if(.not.nprof==nz)then
  print*,"WARNING: input profile length does not match nz"
end if

!get the var id
call check( nf90_inq_varid(ncid, "prof", varid) )

!read the data.
call check( nf90_get_var(ncid, varid, prof) )

!close the file
call check( nf90_close(ncid) )

end subroutine read_nc
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine read_sfc_nc(isst, iseaice)
!read in a single vertical profile from ifile .nc
real(kind=8),intent(out),dimension(imnx,imny) :: isst, iseaice
integer :: ncid, varid, dimid, lenfile

!open netcdf
call check( nf90_open(sfc_ifile, NF90_NOWRITE, ncid) )

!check dimensions
call check( nf90_inq_dimid(ncid, "x", dimid) ) !assume name of x-dim is x
call check( nf90_inquire_dimension(ncid, dimid, len=lenfile))
if( .not. lenfile == imnx )then
  print*,"WARNING: input x-dimension length does not match mosaic nx:", imnx
end if

call check( nf90_inq_dimid(ncid, "y", dimid) ) !assume name of y-dim is y
call check( nf90_inquire_dimension(ncid, dimid, len=lenfile))
if( .not. lenfile == imny )then
  print*,"WARNING: input y-dimension length does not match mosaic ny:", imny
end if

! read SSTs
call check( nf90_inq_varid(ncid, "sst", varid) ) ! assume variable "sst" contains the SSTs
call check( nf90_get_var(ncid, varid, isst) )

! read SIC
call check( nf90_inq_varid(ncid, "seaice", varid) ) ! assume variable "seaice" contains the SIC
call check( nf90_get_var(ncid, varid, iseaice) )

!close the file
call check( nf90_close(ncid) )

end subroutine read_sfc_nc
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine check(status)
    !checking for status during netcdf io 
    integer, intent(in) :: status
    if(status /= nf90_noerr) then 
      print *, trim(nf90_strerror(status))
      stop "Stopped"
    end if
end subroutine check  
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine print_profile_m(pp,uu,dz)
!print center profile of jet and pressure
  real(kind=8),intent(in) :: pp(ny,nz),uu(ny,nz),dz
  real(kind=8) :: z
  integer :: k
  print*," "
  print*,"vertical profile, @center after solving matrix: hgt,pp,uu"
  print*,"---------------------------------------------------------"
  do k=1,nz
    z=(k-1)*dz
    print'(F10.3,F12.3,F8.3)',z,pp(int(ny/2),k),uu(int(ny/2),k)
  enddo
end subroutine print_profile_m
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine print_profile_j(uu,dz)
!print center profile of jet 
  real(kind=8),intent(in) :: uu(ny,nz),dz
  real(kind=8) :: z
  integer :: k
    print*," "
    print*,"vertical profile, @center from get_jet: hgt,uu"
    print*,"----------------------------------------------"
    do k=1,nz
      z=(k-1)*dz
      print'(F10.3,F10.3,F12.3)',z,uu(int(ny/2),k)
    enddo
end subroutine print_profile_j
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine print_profile_bc(th,pp,rho,dz)
!print vertical boundary conditions profile
  real(kind=8),intent(in) :: th(nz),pp(nz),rho(nz),dz
  real(kind=8) :: z
  integer :: k
    print*," "
    print*,"vertical profile from BC: hgt,th,p,rho"
    print*,"----------------------------------"
    do k=1,nz
      z=(k-1)*dz
      print'(F10.3,F10.3,F12.3,F10.4)',z,th(k),pp(k),rho(k)
    enddo
end subroutine print_profile_bc
!---------------------------------------------------------------------------




!---------------------------------------------------------------------------
subroutine print_corr_and_sst(sst,ff)
!print vertical boundary conditions profile
  real(kind=8),intent(in),dimension(nx,ny) :: sst
    real(kind=8),intent(in),dimension(ny) :: ff
  integer :: j
    print*," "
    print*,"horizontal: corriolis, sst"
    print*,"----------------------------------"
    do j=1,ny
      print'(F10.7,F10.3)',ff(j), sst(1,j)
    enddo
end subroutine print_corr_and_sst
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine print_sounding(uu,pp,rho,tt,th,rh,dz)
!print vertical boundary conditions profile
  real(kind=8),intent(in),dimension(ny,nz) :: uu,pp,rho,tt,rh,th
  real(kind=8),intent(in) :: dz
  real(kind=8) :: z
  integer :: k
    print*," "
    print*,"sounding, @center,before pert: hgt,pp,rho,ttv,thv,uu,rh"
    print*,"-------------------------------------------------------"
    do k=1,nz
      z=(k-1)*dz
      print'(F10.3,F12.3,F8.3,F10.3,F10.3,F10.3,F10.3)',z,pp(int(ny/2),k),&
          rho(int(ny/2),k),tt(int(ny/2),k),th(int(ny/2),k),uu(int(ny/2),k),rh(int(ny/2),k)
    enddo
end subroutine print_sounding
!---------------------------------------------------------------------------


!---------------------------------------------------------------------------
subroutine print_sounding2(uu,pp,rho,tt,rh,th,thv,ttv,qq,zz)
!print vertical boundary conditions profile
  real(kind=8),intent(in),dimension(ny,nz) :: uu,pp,rho,tt,rh,th,thv,ttv,qq,zz
  integer :: k
    print*," "
    print*,"sounding, @center pert,after pert: hgt,pp,rho,tt,uu,rh,qq,th,thv,ttv"
    print*,"---------------------------------------------------------------------"
    do k=1,nz
      print'(F9.3,F11.3,F6.3,F9.3,F8.3,F7.3,F7.3,F9.3,F9.3,F9.3)',zz(int(ny/2),k),pp(int(ny/2),k),&
          rho(int(ny/2),k),tt(int(ny/2),k),uu(int(ny/2),k),rh(int(ny/2),k),&
            qq(int(ny/2),k),th(int(ny/2),k),thv(int(ny/2),k),ttv(int(ny/2),k)
    enddo
end subroutine print_sounding2
!---------------------------------------------------------------------------


end module io
