!-------------------------------------------------------------
! Module containing some additional subroutines
!   
! For more info see the README file
!--------------------------------------------------------------


module func

USE const 
USE io
USE init
USE moist

implicit none


CONTAINS 


!-------------------------------------------------------------------
! Repeat the the 2D-setup in a 3D-array
subroutine make_3D(aa)
   real(kind=8), intent(inout), dimension(nx,ny,nz) :: aa
   
   do i=2,nx
      aa(i,:,:) = aa(1,:,:)
   enddo
end subroutine
!-------------------------------------------------------------------


!------------------------------------------------------------------------------
! Calculate Coriokis parameter, where ff=f in the center of the domain
subroutine get_coriolis(ff,ee)
    real(kind=8),intent(out),dimension(ny) :: ff,ee
   
    do j=1,ny
       ff(j) = f + beta*dy*(j-(ny+1)/2.)
       ee(j) = beta
    enddo
end subroutine
!---------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! SST defined by constant offset to initial unperturbed lowest-level temperatures, no sea ice
! The offset can vary over time from sst_diff to sst_diff2
subroutine get_sfc_by_offset(sst,seaice,tt)
   real(kind=8), intent(in), dimension(nx,ny) :: tt
   real(kind=8), intent(out), dimension(mnx,mny):: sst, seaice
   
   ! Interpolation from staggered initJET to unstaggered grid
   do j = 1,mny
      do i = 1,mnx
         sst(i,j) = (tt(i,j)+tt(i+1,j)+tt(i,j+1)+tt(i+1,j+1))/4 + sst_diff
      end do
   end do
   seaice = 0.0
end subroutine
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Surface conditions defined by externally provided high-resolution sea ice and ssts 
!
! There's no actual interpolation in here, so the input resolution must be an integer 
! multiple of the initJET (unstaggered) grid dimensions.
! 
! In addition the routine can (optionally) add a given number of grid cells around all 
! lateral boundaries as padding. This is useful, for example, if initial and boundary 
! conditions are adapted to sea-ice while the model interior is mostly ice-free.
subroutine get_sfc_external(sst, seaice)
   real(kind=8), intent(out), dimension(mnx,mny) :: sst, seaice
   real(kind=8), allocatable, dimension(:,:) :: isst, iseaice
   real(kind=8) :: minsst, maxice, frac
   integer :: iia, iiz, ija, ijz

   allocate(isst(imnx, imny), iseaice(imnx, imny))
   
   ! Initialise variables without ice, then modify below
   sst(:,:) = 9.9e9 ! mark uninitialised values
   seaice(:,:) = 0.0
   
   ! Reading input fields and coarsen to initJET grid
   call read_sfc_nc(isst, iseaice)
   do i = 1,mnx-2*wrf_bdr_padding
      do j = 1,mny-2*wrf_bdr_padding
         iia = (i-1)*sfc_mosaic + 1
         iiz = i*sfc_mosaic
         ija = (j-1)*sfc_mosaic + 1
         ijz = j*sfc_mosaic
         
         sst(i+wrf_bdr_padding, j+wrf_bdr_padding) = sum(isst(iia:iiz,ija:ijz))/(sfc_mosaic**2)
         seaice(i+wrf_bdr_padding, j+wrf_bdr_padding) = sum(iseaice(iia:iiz,ija:ijz))/(sfc_mosaic**2)
      end do
   end do

   ! Add boundary padding if so requested
   if ( wrf_bdr_padding .gt. 0 ) then
      maxice = maxval(iseaice)
      minsst = minval(isst)
      ! West bdr padding: all ice
      do i = 1,wrf_bdr_padding
         do j = 1,mny
            seaice(i,j) = maxice
            sst(i,j) = minsst
          end do
      end do
      ! East bdr padding: linear gradient towards ice of depth wrf_bdr_depth
      do i = mnx-wrf_bdr_padding+1,mnx
         do j = 1,mny
            frac = max(0.0, (i-0.5-mny+wrf_bdr_depth)/wrf_bdr_depth)
            seaice(i,j) = maxice * frac + seaice(mnx-wrf_bdr_padding,j) * (1 - frac)
            sst(i,j) = sst(mnx-wrf_bdr_padding,j)*(1-frac) + minsst*frac
         end do
      end do
      ! South bdr padding: linear gradient towards ice
      do i = 1,mnx
         do j = 1,wrf_bdr_padding
            frac = max(0.0, (wrf_bdr_depth+0.5-j)/wrf_bdr_depth)
            seaice(i,j) = max(seaice(i,j), maxice * frac + seaice(i,wrf_bdr_padding+1) * (1 - frac))
            sst(i,j) = min(sst(i,j), sst(i,wrf_bdr_padding+1)*(1-frac) + minsst*frac, sst(i,wrf_bdr_padding+1))
         end do
      end do
      ! North bdr padding: linear gradient towards ice
      do i = 1,mnx
         do j = mny-wrf_bdr_padding+1,mny
            frac = max(0.0, (j-0.5-mny+wrf_bdr_depth)/wrf_bdr_depth)
            seaice(i,j) = max(seaice(i,j), maxice * frac + seaice(i,mny-wrf_bdr_padding) * (1 - frac))
            sst(i,j) = min(sst(i,j), sst(i,mny-wrf_bdr_padding)*(1-frac) + minsst*frac, sst(i,mny-wrf_bdr_padding))
         end do
      end do
   end if
   
   deallocate(isst, iseaice)
end subroutine
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
! Derive later surface conditions from initial one (by Hai)
! 
! sfc2(y) = sfc(sfc_scale2*y + sfc_shift2) + diff2
! y is the coordinate from the center of the domain
subroutine get_sfc2_from_sfc(sfc,sfc2, diff2)
   real(kind=8), intent(in) :: sfc(ny), diff2
   real(kind=8), intent(out) :: sfc2(ny)
   real(kind=8), dimension(ny) :: ry, ry1
   integer :: j1,j2

   do j=1,ny
      ry(j) = (j-1)*dy - Ly/2
   enddo  

   !linear interpolation, if x is out of Xarr, y take the value at the boundary
   do j=1,mny
      ry1(j) = (ry(j) - sfc_shift2)/sfc_scale2 ! new coordinate
      if (ry1(j) < ry(1)) then
         sfc2(j) = sfc(1)
      else if (ry1(j)>ry(ny)) then
         sfc2(j) = sfc(ny)
      else
         j1 = 1 + (ry1(j) - ry(1) )/dy          ! j in the old coordinate
         j2=j1+1
         sfc2(j)  = sfc(j1) +  (sfc(j2) - sfc(j1)) * (ry1(j) - ry(j1)) / dy
      endif
     
      sfc2(j) = sfc2(j) + diff2
   enddo 
end subroutine
!------------------------------------------------------------------------------


end module func
